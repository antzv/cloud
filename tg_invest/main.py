import os
import telegram
from controller import s


def bot_invest(request):
    # определяем бота методом telegram.Bot по его токену
    bot = telegram.Bot(token=os.environ["TELEGRAM_TOKEN"])
    if request.method == 'GET':
        # из телеграм объекта получаем идентификатор чата
        chat_id = '-1001528182505'
        # отправляем ответное сообщение по идентификатору чата
        # bot.sendMessage(chat_id=chat_id, text=s)
        bot.sendMessage(chat_id=chat_id, text=s, parse_mode='Markdown')
    return 'okay'
