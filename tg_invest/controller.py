import pandas_gbq
import pandas as pd
from google.oauth2 import service_account
from termcolor import colored, cprint
from datetime import datetime, timedelta
from pytz import timezone


# авторизация в google cloud
project_id = 'data-analyst-319914'
credentials = service_account.Credentials.from_service_account_file('data-analyst-319914-e0151312e8c2.json')
pandas_gbq.context.credentials = credentials
pandas_gbq.context.project = project_id

today = datetime.now(tz=timezone('Europe/Moscow')).date()
start_day = datetime.now(tz=timezone('Europe/Moscow')).date() - timedelta(days=7)


query = '''
with t as (
SELECT *, cast(date AS DATE) as ddate 
FROM `data-analyst-319914.invest.deal` )
SELECT ins.name, operation_type, payment, cast(quantity AS INT) as quantity, portfolio
from t 
LEFT JOIN `data-analyst-319914.invest.instruments` ins ON t.figi = ins.figi 
where ddate > current_date() - JUSTIFY_DAYS(INTERVAL 7 DAY)
order by ddate
'''

# получаем последний день, за которые были загрузки сделок
deal = pd.DataFrame(pandas_gbq.read_gbq(f"{query}"))
# суммы по выплатам
div_sum = deal[['payment']][deal['operation_type'] == 'Dividend'].sum().iloc[0].round()
cup_sum = deal[['payment']][deal['operation_type'] == 'Coupon'].sum().iloc[0].round()
partRepayment_sum = deal[['payment']][deal['operation_type'] == 'PartRepayment'].sum().iloc[0].round()
week_payment = div_sum + cup_sum + partRepayment_sum + 0
# списки выплат по активам
div = deal[['name', 'payment']][deal['operation_type'] == 'Dividend'].groupby('name').sum().reset_index()
dict_div = dict(zip(div.name, div.payment))
cup = deal[['name', 'payment']][deal['operation_type'] == 'Coupon'].groupby('name').sum().reset_index()
cup_div = dict(zip(cup.name, cup.payment))
partRepayment = deal[['name', 'payment']][deal['operation_type'] == 'PartRepayment'].groupby('name').sum().reset_index()
partRepayment_div = dict(zip(partRepayment.name, partRepayment.payment))
# покупки активов
iis_buy = deal[['name', 'quantity']][(deal['operation_type'] == 'Buy') & (deal['operation_type'].notna()) & (deal['portfolio'] == 'IIS')].groupby('name').sum().reset_index()
dict_iis_buy = dict(zip(iis_buy.name, iis_buy.quantity))
tink_buy = deal[['name', 'quantity']][(deal['operation_type'] == 'Buy') & (deal['operation_type'].notna()) & (deal['portfolio'] == 'Tinkoff')].groupby('name').sum().reset_index()
dict_tink_buy = dict(zip(tink_buy.name, tink_buy.quantity))
# продажа активов
iis_sell = deal[['name', 'quantity']][(deal['operation_type'] == 'Sell') & (deal['operation_type'].notna()) & (deal['portfolio'] == 'IIS')].groupby('name').sum().reset_index()
dict_iis_sell = dict(zip(iis_sell.name, iis_sell.quantity))
tink_sell = deal[['name', 'quantity']][(deal['operation_type'] == 'Sell') & (deal['operation_type'].notna()) & (deal['portfolio'] == 'Tinkoff')].groupby('name').sum().reset_index()
dict_tink_sell = dict(zip(tink_sell.name, tink_sell.quantity))


s = f'''
*Отчет за период c* `{start_day}` *по* `{today}`
  
*Изменение стоимости портфеля за неделю:* `... руб.`  
    Лидеры роста: ...
    Лидеры падения: ...
    Изменение за счет курсовой разницы: ...

*Сделки на прошедшей неделе:*  
    *В портфеле Тинькофф:* 
        Покупки: `{dict_tink_buy if bool(dict_tink_buy) else 'Покупок не было'}`  
        Продажи: `{dict_tink_sell if bool(dict_tink_sell) else 'Продаж не было'}` 
    
    *В портфеле ИИС:*
        Покупки: `{dict_iis_buy if bool(dict_iis_buy) else 'Покупок не было'}`  
        Продажи: `{dict_iis_sell if bool(dict_iis_sell) else 'Продаж не было'}`  

*Выплаты за прошедшую неделю:* `{str(week_payment) + ' руб.' if week_payment > 0 else 'Выплат не было'}` 
    Дивиденды: `{div_sum if bool(div_sum) else 0} руб.` 
        По акциям: {dict_div if bool(div_sum) else 'Дивидендов не было'}  
    Купоны: `{cup_sum if bool(cup_sum) else 0} руб.` 
        По облигациям: {cup_div if bool(cup_sum) else 'Выплат по купонам не было'}  
    Частичные погашения облигаций: `{partRepayment_sum if bool(partRepayment_sum) else 0} руб.` 
        По облигациям: {partRepayment_div if bool(partRepayment_sum) else 'Погашений не было'}  
  
*Отчетность:*
    На прошлой неделе: ... 
    На следующей неделе: ...
'''
