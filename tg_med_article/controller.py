import requests
from bs4 import BeautifulSoup
from time import sleep
from datetime import datetime
from googletrans import Translator
import re
import pandas as pd
from auth import project_id
import pandas_gbq

translator = Translator()
site = 'https://pubmed.ncbi.nlm.nih.gov'
count_art = 20
term = ['covid-19']
table_name = 'medicine.articles'


def receiving_articles():
    url = f'https://pubmed.ncbi.nlm.nih.gov/?term={term[0]}&filter=years.2016-2021&sort=date&size={count_art}'
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'lxml')
    articles = soup.findAll('div', class_='docsum-content')
    data = []
    for art in articles:
        href = art.find('a', class_='docsum-title').get('href')
        title_article = translator.translate(art.find('a', class_='docsum-title').text.strip(), dest='ru').text
        authors = art.find('span', class_='docsum-authors full-authors').text.strip()
        snippet = translator.translate(art.find('div', class_='full-view-snippet').text.strip(), dest='ru').text
        # получаем строку с описанием статьи
        date_pp = art.find('span', class_='docsum-journal-citation full-journal-citation').text.strip()
        # вытаскиваем из строки с описанием дату появления на сайте
        s = re.search('[0-9]{1,4}\s[A-z]{1,3}\s[0-9]{1,2}', date_pp)
        if s:
            s1 = s.group(0)
        # преобразуем строку к формату даты
            date = datetime.strptime(s1, '%Y %b %d').date()
        else:
            date = 'дата отсутствует'
        data.append([site + href, title_article, authors, snippet if bool(snippet) else 'Описание отсутствует', str(date), term[0], 0])
    header = ['href', 'title_article', 'authors', 'snippet', 'date', 'term', 'sent_flag']
    # формирую дата фрейм из новых статей
    df = pd.DataFrame(data, columns=header)
    # формирую строку ссылок из фрейма новых статей
    bd_href = str(df['href'].tolist()).replace('[', '').replace(']', '')
    # формирую фрейм из всех статей которые хранятся в БД и совпадают по ссылке с новыми
    df_bq = pd.DataFrame(pandas_gbq.read_gbq(f"SELECT * FROM `{project_id}.{table_name}` where href IN ({bd_href});"))
    # удаляю дубликаты между новыми статьями и старыми и все уникальные новые статьи  добавляю в БД
    df_without_dub = df.append(df_bq).drop_duplicates(subset=['href'], keep=False)
    # записываем статьи в БД
    pandas_gbq.to_gbq(df_without_dub, table_name, project_id=project_id, if_exists='append')


def create_message(title, authors, snippet, date, href):
    message = f'''
    *{str(title).replace('{', '').replace('}', '').replace("'", '')}*
    
    *Авторы:* {str(authors).replace('{', '').replace('}', '').replace("'", '')}
    *Описание:* {str(snippet).replace('{', '').replace('}', '').replace("'", '')}
    *дата выхода статьи:* {str(date).replace('{', '').replace('}', '').replace("'", '')}
    *ссылка на статью:* {str(href).replace('{', '').replace('}', '').replace("'", '')}
'''
    return message


def update_flag():
    pandas_gbq.read_gbq(f"Update `{project_id}.{table_name}` Set sent_flag = 1 where sent_flag = 0;")
