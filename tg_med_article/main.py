import pandas as pd
from controller import table_name, create_message, update_flag, receiving_articles
from auth import project_id
import pandas_gbq
import telegram
import os


def bot_medicine(request):
    # определяем бота методом telegram.Bot по его токену
    bot = telegram.Bot(token=os.environ["TELEGRAM_TOKEN"])
    if request.method == 'GET':
        # получаем из БД все неотправленные статьи по 'covid-19'
        receiving_articles()
        df1 = pd.DataFrame(pandas_gbq.read_gbq(f"SELECT * FROM `{project_id}.{table_name}` where sent_flag = 0 and term = 'covid-19';"))
        update_flag()
        for i in df1.itertuples():
            message = create_message(title={i[2]}, authors={i[3]}, snippet={i[4]}, date={i[5]}, href={i[1]})
            # отправляем ответное сообщение по идентификатору чата [id med chat '-1001579263270'] test chat= '-1001793426450'
            chat_id = '-1001579263270'
            bot.sendMessage(chat_id=chat_id, text=message, parse_mode='Markdown')
    # сделать update отправленных сообщений
    return 'okay'

