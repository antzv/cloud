from tinkoff.invest import Client
from datetime import datetime, timedelta
from pytz import timezone
import pandas as pd
from pycbrf.toolbox import ExchangeRates
import pandas_gbq
from auth import project_id, token, broker_account_id_tf, broker_account_id_iis

start_day_val = pandas_gbq.read_gbq(f"SELECT max(cast(date AS date)) FROM invest.operation", project_id='data-analyst-319914').iat[0, 0]

start_day = datetime.strptime(f'{start_day_val}', '%Y-%m-%d').replace(tzinfo=timezone('Europe/Moscow')) + timedelta(days=1)
end_day = datetime.strptime(str(datetime.now().date()), '%Y-%m-%d').replace(tzinfo=timezone('Europe/Moscow'))


with Client(token) as client:
    oper_tink = client.operations.get_operations(account_id=broker_account_id_tf, from_=start_day, to=end_day)
    oper_iis = client.operations.get_operations(account_id=broker_account_id_iis, from_=start_day, to=end_day)


def port_operation(port_oper, port):
    x = 0
    df = pd.DataFrame(
        columns=['figi', 'date', 'instrument_type', 'currency', 'type', 'payment', 'price', 'quantity', 'portfolio']
    )
    for i in port_oper.operations:
        payment = float(f'{i.payment.units}.{(str(i.payment.nano)[0:2]).replace("-", "")}')
        price = float(f'{i.price.units}.{(str(i.price.nano)[0:2]).replace("-", "")}')
        usd = float(ExchangeRates(str(datetime.date(i.date)))['USD'].value)
        eur = float(ExchangeRates(str(datetime.date(i.date)))['USD'].value)

        if i.currency == 'usd':
            payment = payment * usd
            price = price * usd
        elif i.currency == 'eur':
            payment = payment * eur
            price = price * eur
        df.loc[x] = [i.figi, str(datetime.date(i.date)), i.instrument_type, i.currency, i.type,
                     float(payment.__round__(2)), float(price.__round__(2)), int(i.quantity), port]
        x = x + 1
    return df


operation = pd.concat([port_operation(oper_tink, 'Тинькоф'), port_operation(oper_iis, 'ИИС')]).astype({'price': 'float', 'payment': 'float', 'quantity': 'int64'})


# функция записи операций
def operation_write():
    if operation.empty:
        pass
    else:
        pandas_gbq.to_gbq(operation, 'invest.operation', project_id=project_id, if_exists='append')
