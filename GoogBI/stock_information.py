import investpy
import pandas as pd
import pandas_gbq
from auth import project_id
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from bs4 import BeautifulSoup
# from urllib.request import Request, urlopen
from time import sleep

session = requests.Session()
retry = Retry(connect=3, backoff_factor=0.5)
adapter = HTTPAdapter(max_retries=retry)
session.mount('http://', adapter)
session.mount('https://', adapter)


header = ['ticker', 'country', 'tag', 'exchange', 'Industry', 'Sector', 'about_com']

df_investing = pd.DataFrame(columns=header)

# income_statement = pd.DataFrame(columns=['Date', 'Total Revenue', 'Gross Profit', 'Operating Income', 'Net Income', 'ticker'])
# cash_flow_statement = pd.DataFrame(columns=['Date', 'Cash From Operating Activities', 'Cash From Investing Activities', 'Cash From Financing Activities', 'Net Change in Cash', 'ticker'])
# balance_sheet = pd.DataFrame(columns=['Date', 'Total Assets', 'Total Liabilities', 'Total Equity', 'ticker'])

df = pd.DataFrame(pandas_gbq.read_gbq(f"SELECT ticker FROM invest.portfolio where instrument_type = 'Stock'"))

# обнуляем счетчик
x = 0
# цикл получения показателей
for i in df.itertuples():
    try:
        search_result = investpy.search_quotes(text=f'{i[1]}', countries=['russia', 'united states'], products=['stocks'], n_results=1)
        stock_inf = investpy.get_stock_information(stock=search_result.symbol, country=search_result.country, as_json=True)
        url = f'https://ru.investing.com{search_result.tag}-company-profile'
        r = session.get(url, headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(r.text, 'html.parser')
        about_com = soup.find('p', {'id': 'profile-fullStory-showhide'})
        com_info = soup.findAll('div', class_='companyProfileHeader')
        info = list(com_info)[0].text.replace('Отрасль', '').replace('Сектор', '').split('\n')
        df_investing.loc[x] = [search_result.symbol, search_result.country, search_result.tag, search_result.exchange, info[1], info[2], about_com.text if bool(about_com) else '']
        sleep(5)

        # собираем таблицу 'income_statement'

        # собираем таблицу 'cash_flow_statement'

        # собираем таблицу 'balance_sheet'
        x = x + 1
    except Exception:
        continue

# объединение отчетов
# df_stock_financial_summary = income_statement.merge(cash_flow_statement, how='inner', on=['Date', 'ticker'])
# df_stock_financial_summary = df_stock_financial_summary.merge(balance_sheet, how='inner', on=['Date', 'ticker'])
# df_stock_financial_summary = df_stock_financial_summary.rename(columns={'Total Revenue': 'Total_Revenue', 'Gross Profit': 'Gross_Profit', 'Operating Income': 'Operating_Income',
#                                            'Net Income': 'Net_Income', 'Cash From Operating Activities': 'Cash_From_Operating_Activities', 'Cash From Investing Activities': 'Cash_From_Investing_Activities',
#                                            'Cash From Financing Activities': 'Cash_From_Financing_Activities', 'Net Change in Cash': 'Net_Change_in_Cash',
#                                            'Total Assets': 'Total_Assets', 'Total Liabilities': 'Total_Liabilities', 'Total Equity': 'Total_Equity'
#                                         })

# pd.set_option('display.max_columns', None)
# print(df_investing)


# def stock_financial_summary():
#     pandas_gbq.to_gbq(df_stock_financial_summary, 'invest.stock_financial_summary', project_id=project_id, if_exists='replace')
#
#


def stock_information():
    pandas_gbq.to_gbq(df_investing, 'invest.stock_information', project_id=project_id, if_exists='replace')



