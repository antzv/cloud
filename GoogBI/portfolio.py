from tinkoff.invest import Client
import pandas as pd
from datetime import datetime
from pytz import timezone
from pycbrf.toolbox import ExchangeRates
from auth import project_id, token, broker_account_id_tf, broker_account_id_iis
import pandas_gbq

tab_portfolio = 'invest.portfolio'
tab_portfolio_value = 'invest.portfolio_value'
today = datetime.now(tz=timezone('Europe/Moscow'))


# запрашиваем курсы валют через ЦБ
curs = ExchangeRates(str(today.date()))
usd = float(curs['USD'].value)
eur = float(curs['EUR'].value)

# Обращаемся к сервису
with Client(token) as client:
    acc = client.users.get_accounts()
    port_tink = client.operations.get_portfolio(account_id=broker_account_id_tf)
    port_iis = client.operations.get_portfolio(account_id=broker_account_id_iis)


def series_position(portfolio):
    port = portfolio
    shares = port.total_amount_shares
    bonds = port.total_amount_bonds
    etf = port.total_amount_etf
    currencies = port.total_amount_currencies
    return pd.Series(
        data=[
            shares.units + float(f'0.{str(shares.nano)[0:2]}'),
            bonds.units + float(f'0.{str(bonds.nano)[0:2]}'),
            etf.units + float(f'0.{str(etf.nano)[0:2]}'),
            currencies.units + float(f'0.{str(currencies.nano)[0:2]}')
        ]
    )


d = {
    'portfolio': 'Tinkoff',
    'instrument_type': ['shares', 'bonds', 'etf', 'currencies'],
    'position': series_position(port_tink),
    'date': str(today.date())
}
d1 = {
    'portfolio': 'IIS',
    'instrument_type': ['shares', 'bonds', 'etf', 'currencies'],
    'position': series_position(port_iis),
    'date': str(today.date())
}

portfolio_value = pd.concat([pd.DataFrame(d), pd.DataFrame(d1)])


def share(figi):
    # global client, cl, ticker, name, country, sector, exchange
    with Client(token) as client:
        cl = client.instruments
        sh = cl.share_by(id_type=1, id=figi).instrument
        country = sh.country_of_risk_name
        sector = sh.sector
        exchange = sh.exchange
        ticker = sh.ticker
        name = sh.name
        return [country, sector, exchange, ticker, name]


def bonds(figi):
    with Client(token) as client:
        cl = client.instruments
        bond = cl.bond_by(id_type=1, id=figi).instrument
        ticker = bond.ticker
        name = bond.name
        country = bond.country_of_risk_name
        sector = bond.sector
        exchange = bond.exchange
        return [country, sector, exchange, ticker, name]


def etfs(figi):
    global client, cl, ticker, name, country, sector, exchange
    with Client(token) as client:
        cl = client.instruments
        etf = cl.etf_by(id_type=1, id=figi).instrument
        ticker = etf.ticker
        name = etf.name
        country = etf.country_of_risk_name
        sector = etf.sector
        exchange = etf.exchange
        return [country, sector, exchange, ticker, name]


def curs(figi):
    global client, cl, ticker, name, country, sector, exchange
    with Client(token) as client:
        cl = client.instruments
        cur = cl.currency_by(id_type=1, id=figi).instrument
        ticker = cur.ticker
        name = cur.name
        country = cur.country_of_risk_name
        # sector = cur.sector
        exchange = cur.exchange
        return [country, None, exchange, ticker, name]


def portfolio(port, port_name):
    x = 0
    df1 = pd.DataFrame(
        columns=['country', 'sector', 'exchange', 'portfolio', 'instrument_type', 'figi', 'ticker', 'name',  'currency', 'lots', 'quantity',
                 'average_position_price', 'price', 'balance', 'expected_yield', 'current_nkd'])

    for i in port.positions:
        figi = i.figi
        instrument_type = i.instrument_type
        quantity = i.quantity.units
        lots = i.quantity_lots.units
        currency = i.current_price.currency
        expected_yield = float(f'{i.expected_yield.units}.{(str(i.expected_yield.nano)[0:2]).replace("-", "")}')
        average_position_price = float(f'{i.average_position_price.units}.{(str(i.average_position_price.nano)[0:2]).replace("-", "")}')
        current_nkd = float(f'{i.current_nkd.units}.{(str(i.current_nkd.nano)[0:2]).replace("-", "")}')
        price = float(f'{i.current_price.units}.{(str(i.current_price.nano)[0:2]).replace("-", "")}')
        balance = (quantity * price).__round__(2)
        if instrument_type == 'share':
            y = share(figi)
        elif instrument_type == 'bond':
            y = bonds(figi)
        elif instrument_type == 'etf':
            y = etfs(figi)
        elif instrument_type == 'currency':
            y = curs(figi)

        df1.loc[x] = [y[0], y[1], y[2], port_name, instrument_type, figi, y[3], y[4], currency, lots, quantity,
                      average_position_price, price, balance, expected_yield, current_nkd]
        x = x + 1

    return df1


# объединяем основной портфель и ИИС
all_portfolio = pd.concat([portfolio(port_tink, 'Тинькоф'), portfolio(port_iis, 'ИИС')]).reset_index()
# Переводим баланс и  прибыль по акциям и ETF в рубли по текущему курсу
all_portfolio.loc[(all_portfolio['currency'] == 'usd') & ((all_portfolio['instrument_type'] == 'share') | (all_portfolio['instrument_type'] == 'etf')), 'balance'] \
    = (usd * all_portfolio['price'] * all_portfolio['quantity']).__round__(2)
all_portfolio.loc[(all_portfolio['currency'] == 'eur') & ((all_portfolio['instrument_type'] == 'share') | (all_portfolio['instrument_type'] == 'etf')), 'balance'] \
    = (eur * all_portfolio['price'] * all_portfolio['quantity']).__round__(2)
all_portfolio.loc[(all_portfolio['currency'] == 'usd') & ((all_portfolio['instrument_type'] == 'share') | (all_portfolio['instrument_type'] == 'etf')), 'expected_yield'] \
    = (usd * all_portfolio['expected_yield']).__round__(2)
all_portfolio.loc[(all_portfolio['currency'] == 'eur') & ((all_portfolio['instrument_type'] == 'share') | (all_portfolio['instrument_type'] == 'etf')), 'expected_yield'] \
    = (eur * all_portfolio['expected_yield']).__round__(2)


# функция записи портфелей
def portfolio_write():
    pandas_gbq.to_gbq(all_portfolio, tab_portfolio, project_id=project_id, if_exists='replace')


# # функция записи стоимости портфелей на дату
def portfolio_value_write():
    max_day = pandas_gbq.read_gbq(f"SELECT max(cast(date AS date)) FROM invest.portfolio_value", project_id='data-analyst-319914').iat[0, 0]
    if max_day == datetime.now().date():
        pass
    else:
        pandas_gbq.to_gbq(portfolio_value, tab_portfolio_value, project_id=project_id, if_exists='append')